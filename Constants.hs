module Constants where

width, height, offset :: Int
width = 300
height = 300
offset = 100

-- | Number of frames to show per second.
fps :: Int
fps = 60

